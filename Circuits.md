
# Circuits

## Introduction
*Voltage* is measured as the potential difference between two nodes, and the *current* is the flow of charge across a branch. These quantities are given by 

```math
v = dw/dq \quad\text{and}\quad i = dq/dt
``` 
respectively.

The *passive sign convention* (PSC) is the penultimate concept of circuit analysis, defined as:

> If the reference current is in the same direction as the reference voltage drop across an element, the relationship is positive.

Power relates voltage and current such that $$p = dw/dt = vi$$ This suggests if $p > 0$, power is being delivered to the element(s). Else, power is being extracted. Thus, $\sum p = 0$.

## Voltage & Current Sources
Voltage sources in parallel and current sources in series are only permissible if they are equal.

> Kirchoff's laws state the sum of currents at a node is zero (KCL), as is the sum of voltages around a loop (KVL).

Conventionally, currents entering a node are positive and those leaving are negative. Additionally, the loops are traversed in a clockwise direction.

For resistors in series between nodes $a$ and $b$, the voltage division equation gives the specific voltage $v_i$ for one of the resistors $R_i$ via $$v_i = iR_i = v R_i / R_{eq} $$

Likewise, the current division equation uses the current for resistors in series via $$i_i = v/R_i = i R_{eq}/R_i$$

> These equations are viable as voltage across branches in parallel is equal, as is the current through nodes in series.

<img src="/assets/Screen%20Shot%202018-10-16%20at%201.14.34%20AM.png" width="340"  hspace="45"/> <img src="/assets/Screen%20Shot%202018-10-16%20at%201.14.46%20AM.png" width="255"/>
